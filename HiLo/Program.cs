﻿using System;

namespace HiLo
{
    class Program
    {
        static void Main(string[] args)
        {
            int randomNumber = new Random().Next(101);
            Console.WriteLine(randomNumber);
            while (true)
            {
                Console.WriteLine("Guess a number: ");
                int input = int.Parse(Console.ReadLine());
                if (input == randomNumber)
                {
                    break;
                }
                else if (input < randomNumber)
                {
                    Console.WriteLine("Guess is to low");
                }
                else if (input > randomNumber)
                {
                    Console.WriteLine("Guess is to high");
                }
            }
            Console.WriteLine("You win!");
            Console.ReadKey();
        }
    }
}
